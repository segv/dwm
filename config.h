#define THEME "themes/tokyonight.h"
#include THEME
/* See LICENSE file for copyright and license details. */


/* appearance */
static const unsigned int borderpx = 2;
static const unsigned int gappx     = 0;
static const unsigned int snap      = 10;       /* snap pixel */
static const int showbar = 1;
static const int topbar = 0;

/* if (usr_bh == 0) then bh = auto else bh = usr_bh*/
static const int user_bh = 22;

static const char *fonts[] = {
	"InconsolataLGC Nerd Font:pixelsize=14:bold:antialias=true:autohint=true",
	"Noto Color Emoji:bold:antialias=true"
};

static const char *colors[][3] = {
	/*                  fg          bg	border   */
	[SchemeNorm]	= { white,	black,	black },
	[SchemeSel]	= { black,	blue,	green },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8" };

#define TAG(x) (1 << (x - 1))
static const Rule rules[] = {
	/* xprop(1):
	 *      WM_CLASS(STRING) = instance, class
	 *      WM_NAME(STRING) = title
	 */
	/* class			instance	title			tags mask   isfloating  monitor	float x,y,w,h			floatborderpx*/
	{ "st-256color",	NULL,		"dropdown",		NULL,		1,			-1, 		283,	184, 800, 400,	10},
};

/* layout(s) */
static const float mfact = 0.6; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1; /* number of clients in master area */
static const int resizehints =
	1; /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]", tile }, /* first entry is default */
};

static const char* uncloseable = "acme";

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                      \
	{ MODKEY,				KEY,	view,		{ .ui = 1 << TAG } },\
	{ MODKEY | ControlMask,			KEY,	toggleview,	{ .ui = 1 << TAG } },\
	{ MODKEY | ShiftMask,			KEY,	tag,		{ .ui = 1 << TAG } },\
	{ MODKEY | ControlMask | ShiftMask,	KEY,	toggletag,	{ .ui = 1 << TAG } }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                                             \
	{                                                                      \
		.v = (const char *[])                                          \
		{                                                              \
			"/bin/sh", "-c", cmd, NULL                             \
		}                                                              \
	}

#include "movestack.c"
#include "shift-tools.c"

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,		XK_b,		togglebar,	{ 0 } },
	{ MODKEY,		XK_q,		killclient,	{ 0 } },

	{ MODKEY | ShiftMask,	XK_j,		movestack,	{ .i = +1 } },
	{ MODKEY | ShiftMask,	XK_k,		movestack,	{ .i = -1 } },
	{ MODKEY,		XK_j,		focusstack,	{ .i = +1 } },
	{ MODKEY,		XK_k,		focusstack,	{ .i = -1 } },

	{ MODKEY,		XK_h,		shiftview, 	{ .i = -1 } },
	{ MODKEY,		XK_l,		shiftview, 	{ .i = +1 } },
	{ MODKEY | ShiftMask,	XK_h,		setmfact,	{ .f = -0.02 } },
	{ MODKEY | ShiftMask,	XK_l,		setmfact,	{ .f = +0.02 } },

	{ MODKEY | ControlMask,	XK_h,		shiftboth,	{ .i = -1 } },
	{ MODKEY | ControlMask,	XK_l,		shiftboth,	{ .i = +1 } },

	{ MODKEY,		XK_comma,	focusmon,	{ .i = -1 } },
	{ MODKEY,		XK_period,	focusmon,	{ .i = +1 } },
	{ MODKEY | ShiftMask,	XK_comma,	tagmon,		{ .i = -1 } },
	{ MODKEY | ShiftMask,	XK_period,	tagmon,		{ .i = +1 } },

	TAGKEYS(XK_1, 0),
	TAGKEYS(XK_2, 1),
	TAGKEYS(XK_3, 2),
	TAGKEYS(XK_4, 3),
	TAGKEYS(XK_5, 4),
	TAGKEYS(XK_6, 5),
	TAGKEYS(XK_7, 6),
	TAGKEYS(XK_8, 7),

	{ MODKEY,		XK_minus,	setgaps,        {.i = -1 } },
	{ MODKEY,		XK_equal,	setgaps,        {.i = +1 } },
	{ MODKEY | ShiftMask,	XK_equal,	setgaps,        {.i = 0  } },

	{ MODKEY,		XK_f,		togglefullscr, 	{ 0 } },
	{ MODKEY,		XK_0,		incnmaster, 	{ .i = +1 } },
	{ MODKEY,		XK_9,		incnmaster, 	{ .i = -1 } },
	{ MODKEY,		XK_s,		togglesticky, 	{ 0 } },
	{ MODKEY | ShiftMask,	XK_r,		spawn, SHCMD("killall -s SIGUSR1 sxhkd && notify-send '⌨ Hotkey daemon reloaded!' || notify-send -u critical '⌨ Failed to reload kotkey daemon!'") },

	{ MODKEY | ControlMask, XK_t,		setlayout,      {.v = &layouts[0]} },
	{ MODKEY | ShiftMask,	XK_f,		togglefloating, {0} },
	{ MODKEY,               XK_z,		zoom,           {0} },

	{ MODKEY,     		XK_Menu,	focusstack,	{ .i = +1 }},
};

static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

